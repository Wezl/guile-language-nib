(define-module (language nib generic)
  #:use-module (oop goops)
  #:use-module (srfi srfi-43)
  #:export (call on-scalars on-scalars-binary))

(define-generic call)

(define-method (call (fn <procedure>) args)
  (fn args))
(define-method (call (fn <generic>) args)
  (fn args))

(define-method (call (v <vector>) (index <integer>))
  (vector-ref v index))
(define-method (call (v <vector>) arg)
  (on-scalars (lambda (n) (vector-ref v n)) arg))

(define-method (call (s <string>) (index <integer>))
  (string-ref s index))
(define-method (call (v <vector>) arg)
  (on-scalars (lambda (n) (string-ref s n)) arg))

(define-generic scalar?)
(define-method (scalar? (s <string>)) #f)
(define-method (scalar? (v <vector>)) #f)
(define-method (scalar? (p <procedure>)) #f)
(define-method (scalar? (p <generic>)) #f)

(define-generic on-scalars)
(define-method (on-scalars fn (v <vector>))
  (vector-map (lambda (e) (on-scalars fn e)) v))
(define-method (on-scalars fn (s <string>))
  (vector-map fn (string->vector s)))
(define-method (on-scalars fn (other <procedure>))
  (lambda (a) (on-scalars fn (other a))))
(define-method (on-scalars fn (other <generic>))
  (lambda (a) (on-scalars fn (other a))))
(define-method (on-scalars fn val)
  (call fn val))

(define-generic generic-map-2)
(define-method (generic-map-2 fn (a <vector>) (b <vector))
  (vector-map (lambda (an bn) (generic-map-2 fn an bn)) a b))
;(define-method (generic-map-2 fn )) TODO

(define (on-scalars-binary fn val)
  (on-scalars (lambda (pair) (generic-map-2 fn (car pair) (cdr pair))) val))

